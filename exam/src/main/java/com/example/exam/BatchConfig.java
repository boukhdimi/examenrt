package com.example.exam;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.DataClassRowMapper;
import org.springframework.jdbc.support.JdbcTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    private KafkaToCSV kafkaToCSVItemWriter;

    @Autowired
    private DataSource dataSource; // Configure your DataSource

    @Value("${custom.query}")
    private String customQuery;


    @Bean
    public JdbcCursorItemReader<JobCandidate> dataTableReader(
            DataSource dataSource)

    {
        String sql = "select * from JOB_CANDIDATE";
        return new JdbcCursorItemReaderBuilder<JobCandidate>()
                .name("billingDataTableReader")
                .dataSource(dataSource)
                .sql(sql)
                .rowMapper(new DataClassRowMapper<>(JobCandidate.class))
                .build();
    }
    @Bean
    public ItemProcessor<JobCandidate, JobCandidate> customItemProcessor() {
        return new CustomItemProcessor(customQuery);
    }

    @Bean
    public Step myStep(JobRepository jobRepository,JdbcTransactionManager transactionManager,
                       JdbcCursorItemReader<JobCandidate> dataTableReader
                       ) {
        return new StepBuilder("dbIngestion", jobRepository)
                .<JobCandidate, JobCandidate>chunk(100,transactionManager)
                .reader(dataTableReader)
                .writer((ItemWriter<? super JobCandidate>) kafkaToCSVItemWriter)
                .build();
    }

    @Bean
    public Job myJob(JobRepository jobRepository, Step myStep) {
        return new JobBuilder("BillingJob",jobRepository)
                .start(myStep)
                .build();
    }
}
