package com.example.exam;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import java.util.Date;

public class CustomItemProcessor implements ItemProcessor<JobCandidate, JobCandidate> {

    private String customQuery;

    // Constructor to inject customQuery
    public CustomItemProcessor(String customQuery) {
        this.customQuery = customQuery;
    }

    @Override
    @Retryable(value = { Exception.class }, maxAttempts = 3, backoff = @Backoff(delay = 3000))
    public JobCandidate process(JobCandidate item) throws Exception {
        try {
            // Validate data types
            validateDataTypes(item);



            // Simulate an error for testing purposes
            if (Math.random() < 0.07) {
                throw new RuntimeException("Simulated processing error");
            }

            return item;
        } catch (Exception e) {

            return null; // Returning null indicates that the item should be skipped
        }
    }

    private void validateDataTypes(JobCandidate item) {
        if (item.businessEntityID() < 0) {
            throw new IllegalArgumentException("Invalid BusinessEntityID");
        }

        if (item.departmentID() < 0) {
            throw new IllegalArgumentException("Invalid departmentID");
        }

        if (item.shiftID() < 0) {
            throw new IllegalArgumentException("Invalid shiftID");
        }

    }
}
