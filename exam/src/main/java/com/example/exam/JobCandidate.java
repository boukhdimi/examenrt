package com.example.exam;

import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.util.Date;

public record JobCandidate(
        int businessEntityID,
        int departmentID,
        int shiftID,
        Date startDate,
        Date endDate,
        Date modifiedDate

) {
}
