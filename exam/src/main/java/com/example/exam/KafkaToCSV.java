package com.example.exam;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class KafkaToCSV {

    private static final String TOPIC = "mytopic";
    private static final String BOOTSTRAP_SERVERS = "localhost:9092";

    public static void main(String[] args) throws IOException {

        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "my-group");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");


        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);


        Writer writer = new FileWriter("data.csv");


        consumer.subscribe(List.of(TOPIC));


        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(1000);

            for (ConsumerRecord<String, String> record : records) {
                // Écrivez le message dans le fichier CSV
                writer.write(record.value() + "\n");
            }
        }



    }
}